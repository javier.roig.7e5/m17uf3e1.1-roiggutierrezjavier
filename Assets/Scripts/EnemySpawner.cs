using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemySpawner : MonoBehaviour
{
    private GameObject Player;
    private float minX, maxX, minY, maxY;
    [SerializeField] private Transform[] points;
    [SerializeField] private GameObject[] enemys;
    [SerializeField] private float timeEnemys;

    private float timeNextEnemys;

    public int killedEnemys;
    [SerializeField] private int finalEnemys;

    private bool spawnerActive = false;
    private int spawnLimit = 1;
    private bool waveActive = false;
    private int enemysSpawnedCounter;
    private bool spawnAvailable = true;
    [SerializeField] private GameObject winGame;
    public int totalSpawnedEnemies = 0;
    public int actualWave = 0;
    private void Start()
    {
        Player = GameObject.Find("PlayerPool");
        maxX = points.Max(point => point.position.x);
        minX = points.Min(point => point.position.x);
        maxY = points.Max(point => point.position.z);
        minY = points.Min(point => point.position.z);

        killedEnemys = 0;
        enemysSpawnedCounter = 0;
    }

    private void Update()
    {
        if (!Player.GetComponent<PlayerData>().alreadyDead) {
            if (killedEnemys >= finalEnemys)
            {
                winGame.GetComponent<WinLose>().GameWin = true;
                //Debug.Log("GAME WIN");
            }
            else
            {
                if (enemysSpawnedCounter >= spawnLimit)
                {
                    //Debug.Log("TODOS LOS ENEMIGOS DE LA OLEADA SPAWNEADOS");
                    StartCoroutine(TimeBetweenWaves());
                }

                if (!waveActive && spawnAvailable)
                {
                    //Debug.Log("ESTABLECIENDO OLEADA...");
                    spawnLimit = Random.Range(2, 8);
                    waveActive = true;
                    spawnerActive = true;
                    enemysSpawnedCounter = 0;
                    actualWave++;
                }

                if (spawnerActive)
                {
                    timeNextEnemys += Time.deltaTime;
                    if (timeNextEnemys >= timeEnemys)
                    {
                        //Debug.Log("INTENTANDO SPAWEAR...");
                        timeNextEnemys = 0;
                        CreateEnemy(enemys);
                        enemysSpawnedCounter++;
                        //Debug.Log("ENEMIGO SPAWNEADO");
                        totalSpawnedEnemies += enemysSpawnedCounter;
                    }
                }
            }
        }
    }

    private void CreateEnemy(GameObject[] EnemyArray)
    {
        int enemyNumber = Random.Range(0, EnemyArray.Length);
        Vector3 randomPosition = new Vector3(Random.Range(minX, maxX), 2f, Random.Range(minY, maxY));

        Instantiate(EnemyArray[enemyNumber], randomPosition, Quaternion.identity);
    }

    private IEnumerator TimeBetweenWaves()
    {
        spawnAvailable = false;
        spawnerActive = false;
        enemysSpawnedCounter = 0;
        spawnLimit = 1;
        //Debug.Log("ESPERANDO PARA SIGUIENTE OLEADA...");
        yield return new WaitForSeconds(20);
        //Debug.Log("ESPERA TERMINADA");
        spawnAvailable = true;
        waveActive = false;
    }

    public void CountKills()
    {
        killedEnemys++;
    }
}
