using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class AimController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera aimVirtualCamera;
    public Animator animator;
    public GameObject WeaponObject;
    public GameObject WeaponBack;
    public bool weaponSelected = false;

    [SerializeField] private GameObject scopeCanvas;

    private int aimLayerIndex, changeWeaponLayerIndex;
    int aimLayerWeight = 1, changeWeaponLayerWeight;

    public bool isAiming = false;
    bool weaponChangeCoolDown = false;

    private void Awake()
    {
        //playerMove = GetComponent<PlayerMove>();
        //playerInputs = GetComponent<PlayerInputs>();
    }

    private void Start()
    {
        aimLayerIndex = animator.GetLayerIndex("Not Aiming");
        changeWeaponLayerIndex = animator.GetLayerIndex("Weapon On");
    }

    void Update()
    {
        if (!isAiming)
        {
            if (Input.GetKey(KeyCode.Q) && !weaponChangeCoolDown)
            {
                
                if (weaponSelected)
                {
                    animator.SetTrigger("weaponOut");
                    WeaponObject.gameObject.SetActive(false);
                    WeaponBack.gameObject.SetActive(true);
                    weaponSelected = false;
                }
                else if (!weaponSelected)
                {
                    animator.SetTrigger("weaponOn");
                    WeaponBack.gameObject.SetActive(false);
                    WeaponObject.gameObject.SetActive(true);
                    weaponSelected = true;
                }
                StartCoroutine(WeaponChangeCoolDown());
            }
            else
            {

            }
        }

        if (!this.GetComponent<PlayerMove>().isCrouch)
        {
            if (weaponSelected)
            {
                if (Input.GetButtonDown("Fire2"))
                {
                    isAiming = true;
                    aimVirtualCamera.gameObject.SetActive(true);
                    aimLayerWeight = 0;
                    scopeCanvas.gameObject.SetActive(true);
                }
                else if (Input.GetButtonUp("Fire2"))
                {
                    isAiming = false;
                    aimVirtualCamera.gameObject.SetActive(false);
                    aimLayerWeight = 1;
                    scopeCanvas.gameObject.SetActive(false);
                }
            }
        }

        animator.SetLayerWeight(aimLayerIndex, aimLayerWeight);
    }

    private IEnumerator WeaponChangeCoolDown()
    {
        weaponChangeCoolDown = true;
        //changeWeaponLayerWeight = 1;
        animator.SetBool("weaponTransitionDone", false);
        animator.SetLayerWeight(changeWeaponLayerIndex, 1);
        yield return new WaitForSeconds(2);
        animator.SetBool("weaponTransitionDone", true);
        weaponChangeCoolDown = false;
        animator.SetLayerWeight(changeWeaponLayerIndex, 0);
        //changeWeaponLayerWeight = 0;
    }
}
