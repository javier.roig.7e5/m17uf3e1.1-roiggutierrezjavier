using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinLose : MonoBehaviour
{
    [SerializeField] private GameObject winScreen;
    [SerializeField] private GameObject loseScreen;
    [SerializeField] private GameObject player;

    public bool GameWin = false;
    void Start()
    {
        winScreen.gameObject.SetActive(false);
        loseScreen.gameObject.SetActive(false);
    }

    void Update()
    {
        if (player.GetComponent<PlayerData>().playerDeath)
        {
            loseScreen.gameObject.SetActive(true);
        }else if (GameWin)
        {
            winScreen.gameObject.SetActive(true);
            player.GetComponent<PlayerMove>().Dance();
        }
    }
}
