using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float life = 3;
    void Awake()
    {
        Destroy(gameObject, life); 
    }

    /*private void OnCollisionEnter(Collision collision)
    {
        //Destroy(collision.gameObject);
        Debug.Log("Something Hitted!!");
        Destroy(gameObject);
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            //animator.SetTrigger("Hitted");
            //Debug.Log("Player Hitted!!");
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
