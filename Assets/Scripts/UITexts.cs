using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class UITexts : MonoBehaviour
{
    [SerializeField] private GameObject WaveText;
    [SerializeField] private GameObject KillsText;

    void Update()
    {
        WaveText.gameObject.GetComponent<TMP_Text>().text = Convert.ToString("Oleada: " + this.GetComponent<EnemySpawner>().actualWave);
        KillsText.gameObject.GetComponent<TMP_Text>().text = Convert.ToString("Muertes: " + this.GetComponent<EnemySpawner>().killedEnemys);
    }
}
