using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public float MaxLife = 100;
    public float CurrentLife = 100;
    public bool playerDeath = false;
    public bool alreadyDead = false;
    public Animator animator;
    [SerializeField] private GameObject damageOverlay;
    void Start()
    {
        /*if (CurrentLife < 1 && !alreadyDead)
        {
            playerDeath = true;
            animator.SetFloat("Life", CurrentLife);
            alreadyDead = true;
        }*/
    }

    void Update()
    {
        if (CurrentLife < 1 && !alreadyDead)
        {
            playerDeath = true;
            animator.SetFloat("Life", CurrentLife);
            alreadyDead = true;
            transform.position = new Vector3(transform.position.x, 0.146f, transform.position.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyBullet")
        {
            TakeDamage(15);
            StartCoroutine(DamageOverlayTime());
            Destroy(other.gameObject);
        }
    }

    private void TakeDamage(float hitPoints)
    {
        CurrentLife -= hitPoints;
    }

    private IEnumerator DamageOverlayTime()
    {
        damageOverlay.gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        damageOverlay.gameObject.SetActive(false);
    }
}
