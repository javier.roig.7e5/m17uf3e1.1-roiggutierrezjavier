using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChaser : MonoBehaviour
{
    private GameObject Player;
    private GameObject gameWin;
    public float ChasingSpeed;

    public bool ChaseAvailable = true;

    public float distanceBetween;
    [SerializeField] private float distance;

    public Animator animator;

    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    int attackType;

    public float Health = 5;

    public bool IsDeath = false;

    public bool EnemyAvailable;

    public CapsuleCollider capsuleCollider;

    private bool ShootAvailable = true;

    public GameObject projectile;

    public Transform firePoint;

    public float projectileSpeed = 10f;

    private bool ShootActive = false;

    private GameObject enemySpawner;

    private bool enemyAlreadyDeath = false;

    void Start()
    {
        attackType = Random.Range(0, 2);
        Player = GameObject.Find("PlayerPool");
        enemySpawner = GameObject.Find("EnemySpawner");
        gameWin = GameObject.Find("WinLoseController");
    }


    void Update()
    {
        if (gameWin.GetComponent<WinLose>().GameWin)
        {
            Destroy(gameObject);
        }

        if (Player.GetComponent<PlayerData>().alreadyDead)
        {
            ChaseAvailable = false;
            ShootAvailable = false;
        }

        if (!IsDeath)
        {
            if (Health < 1 && !IsDeath)
            {
                if (attackType == 0)
                {
                    IsDeath = true;
                    int deathAnim = Random.Range(0, 5);
                    animator.SetFloat("Health", 0);
                    animator.SetInteger("DeathAnim", deathAnim);
                    //Debug.Log("DeathAnim number: " + deathAnim);
                }
                else if (attackType == 1)
                {
                    IsDeath = true;
                    int deathAnim = Random.Range(0, 2);
                    animator.SetFloat("Health", 0);
                    animator.SetInteger("DeathAnim", deathAnim);
                    //Debug.Log("DeathAnim number: " + deathAnim);
                }
                StartCoroutine(DeathBodyFallTime());
                DeathBodyDestroyTime();
            }

            if (distance < 5)
            {
                ChaseAvailable = false;
                animator.SetTrigger("Shoot");
                animator.SetInteger("AttackType", attackType);
                ShootActive = true;
            }
            else
            {
                ChaseAvailable = true;
                ShootActive = false;
            }


            distance = Vector3.Distance(transform.position, Player.transform.position);
            Vector3 direction = Player.transform.position - transform.position;
            direction.Normalize();
            animator.SetFloat("Horizontal", direction.x);
            animator.SetFloat("Vertical", direction.z);
            animator.SetFloat("Speed", direction.sqrMagnitude);

            animator.SetFloat("Distance", distance);

            float targetAngle = Mathf.Atan2(direction.z, -direction.x) * Mathf.Rad2Deg - 75f;

            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            if (ChaseAvailable)
            {
                if (distance < distanceBetween)
                {
                    transform.position = Vector3.MoveTowards(this.transform.position, Player.transform.position, ChasingSpeed * Time.deltaTime);
                }
            }

            if (ShootAvailable && !ChaseAvailable)
            {
                var bullet = Instantiate(projectile, firePoint.position, firePoint.rotation);
                bullet.GetComponent<Rigidbody>().velocity = firePoint.forward * projectileSpeed;
                StartCoroutine(Cadence());
            }
        }else if (IsDeath && !enemyAlreadyDeath)
        {
            enemySpawner.GetComponent<EnemySpawner>().CountKills();
            enemyAlreadyDeath = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            //animator.SetTrigger("Hitted");
            TakeDamage(1);
        }
    }

    private void TakeDamage(float damage)
    {
        Health -= damage;
        if (Health < 1)
        {
            if (attackType == 0)
            {
                IsDeath = true;
                int deathAnim = Random.Range(0, 5);
                animator.SetFloat("Health", 0);
                animator.SetInteger("DeathAnim", deathAnim);
                //Debug.Log("DeathAnim number: " + deathAnim);
            }
            else if (attackType == 1)
            {
                IsDeath = true;
                int deathAnim = Random.Range(0, 2);
                animator.SetFloat("Health", 0);
                animator.SetInteger("DeathAnim", deathAnim);
                //Debug.Log("DeathAnim number: " + deathAnim);
            }
            StartCoroutine(DeathBodyFallTime());
            StartCoroutine(DeathBodyDestroyTime());
        }
        else
        {
            animator.SetTrigger("Hitted");
        }
    }

    private IEnumerator DeathBodyFallTime()
    {
        yield return new WaitForSeconds(2);
        if (ShootActive && attackType == 0)
        {
            capsuleCollider.center = new Vector3(capsuleCollider.center.x, 1.78f, capsuleCollider.center.z);
        }else if (ShootActive && attackType == 1)
        {
            capsuleCollider.center = new Vector3(capsuleCollider.center.x, 1.28f, capsuleCollider.center.z);
        }
        //capsuleCollider.center = new Vector3(capsuleCollider.center.x, 1.7f, capsuleCollider.center.z);
        //capsuleCollider.center = new Vector3(capsuleCollider.center.x, 1.13f, capsuleCollider.center.z);
    }

    private IEnumerator DeathBodyDestroyTime()
    {
        yield return new WaitForSeconds(10);
        Destroy(gameObject);
    }

    private IEnumerator Cadence()
    {
        ShootAvailable = false;
        yield return new WaitForSeconds(2);
        ShootAvailable = true;
    }
}
