using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
//using UnityEngine.InputSystem;

public class PlayerMoveIS : MonoBehaviour
{
    public CharacterController controller;
    //private PlayerControls playerInputActions;
    private PlayerInputs playerInputs;

    public Animator animator;
    public Transform cam;
    //[SerializeField] private CinemachineVirtualCamera aimVirtualCamera;

    [Header("Movement")]
    public float speed = 1f;
    public float gravity = -9.10f;
    public float jumpHeight = 1f;

    [Header("Ground Check")]
    public Transform ground_check;
    public float ground_distance = 0.4f;
    public LayerMask ground_mask;

    Vector3 velocity;
    bool isGrounded;

    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    public bool isCrouch = false;
    public bool isDancing = false;
    public bool moveAvailable = true;

    private Vector2 inputVector;

    public float Sensitivity = 1f;

    private void Awake()
    {
        playerInputs = GetComponent<PlayerInputs>();
        /*playerInputActions = new PlayerControls();
        playerInputActions.Enable();
        playerInputActions.Player.Jump.performed += Jump;
        playerInputActions.Player.Sprint.performed += Sprint;
        playerInputActions.Player.Crouch.performed += Crouch;*/
    }

    void Update()
    {
        /*if (playerInputs.aim)
        {
            aimVirtualCamera.gameObject.SetActive(true);
        }
        else
        {
            aimVirtualCamera.gameObject.SetActive(false);
        }*/

        if (playerInputs.crouch)
        {
            if (isCrouch)
            {
                isCrouch = false;
                animator.SetBool("crouch", false);
            }
            else
            {
                isCrouch = true;
                animator.SetBool("crouch", true);
            }
        }

        if (playerInputs.jump)
        {
            //velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            Debug.Log("Debeia SALTAR!!");
        }

        /*if (Input.GetKey(KeyCode.M))
        {
            isDancing = true;
            Debug.Log("IsDancing: " + isDancing);
            isDancing = false;
            StartCoroutine(Dance());
        }*/
        if (this.GetComponent<PlayerData>().playerDeath)
        {
            moveAvailable = false;
        }

        if (moveAvailable)
        {
            isGrounded = Physics.CheckSphere(ground_check.position, ground_distance, ground_mask);

            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }

            /*float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");*/

            //inputVector = playerInputs.move.ReadValue<Vector2>();
            //float speed = 1f;
            //sphereRigidbody.AddForce(new Vector3(inputVector.x, 0, inputVector.y) * speed, ForceMode.Force);

            animator.SetFloat("forward", playerInputs.move.y);
            animator.SetFloat("strafe", playerInputs.move.x);

            Vector3 move = transform.right * playerInputs.move.x + transform.forward * playerInputs.move.y;

            controller.Move(move * speed * Time.deltaTime);

            /*if (Input.GetKeyDown(KeyCode.C))
            {
                if (isCrouch)
                {
                    isCrouch = false;
                    animator.SetBool("crouch", false);
                }
                else
                {
                    isCrouch = true;
                    animator.SetBool("crouch", true);
                }
            }*/

            /*if (Input.GetKey(KeyCode.LeftShift))
            {
                speed = 5f;
                animator.SetBool("run", true);

                Vector3 direction = new Vector3(inputVector.x, 0f, inputVector.y).normalized;

                if (direction.magnitude >= 0.1f)
                {
                    float targetAngle = Mathf.Atan2(direction.z, -direction.x) * Mathf.Rad2Deg - 90f + cam.eulerAngles.y;

                    float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
                    transform.rotation = Quaternion.Euler(0f, angle, 0f);
                    Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                    controller.Move(moveDir.normalized * speed * Time.deltaTime);
                }
            }
            else
            {
                speed = 3f;
                animator.SetBool("run", false);
            }*/

            /*if (Input.GetButtonDown("Jump") && isGrounded)
            {

                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            }*/

            if (isGrounded)
            {
                animator.SetBool("jump", false);
            }
            else
            {
                animator.SetBool("jump", true);
            }

            /*velocity.y += gravity * Time.deltaTime;

            controller.Move(velocity * Time.deltaTime);*/

            /*if (Input.GetKey(KeyCode.R))
            {
                animator.SetBool("roll", true);
            }*/
        }
    }

    private IEnumerator Dance()
    {
        moveAvailable = false;
        animator.SetBool("dance", true);
        yield return new WaitForSeconds(19);
        moveAvailable = true;
    }

    /*public void Jump(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
    }

    public void Sprint(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            speed = 5f;
            animator.SetBool("run", true);

            Vector3 direction = new Vector3(inputVector.x, 0f, inputVector.y).normalized;

            if (direction.magnitude >= 0.1f)
            {
                float targetAngle = Mathf.Atan2(direction.z, -direction.x) * Mathf.Rad2Deg - 90f + cam.eulerAngles.y;

                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);
                Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                controller.Move(moveDir.normalized * speed * Time.deltaTime);
            }
        }
        if (context.canceled)
        {
            speed = 3f;
            animator.SetBool("run", false);
        }
        
    }

    public void Crouch(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (isCrouch)
            {
                isCrouch = false;
                animator.SetBool("crouch", false);
            }
            else
            {
                isCrouch = true;
                animator.SetBool("crouch", true);
            }
        }
    }*/
}
