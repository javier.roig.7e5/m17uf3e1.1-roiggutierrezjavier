using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class VCamRotation : MonoBehaviour
{
    public float velocidadRotacion = 2f;

    private CinemachinePOV pov;

    void Start()
    {
        // Obtener el componente CinemachinePOV de la c�mara virtual
        pov = GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachinePOV>();
    }

    void Update()
    {
        // Obtener el movimiento del rat�n en los ejes X e Y
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        // Rotar la c�mara virtual seg�n el movimiento del rat�n
        pov.m_VerticalAxis.Value += mouseY * velocidadRotacion;
        pov.m_HorizontalAxis.Value += mouseX * velocidadRotacion;
    }
}
