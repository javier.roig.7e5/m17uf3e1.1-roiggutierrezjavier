using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillHealthBar : MonoBehaviour
{
    [SerializeField] private GameObject player;
    public Image fillImage;
    [SerializeField] private Slider slider;

    void Awake()
    {
        slider = GetComponent<Slider>();
    }

    void Start()
    {
    }

    void Update()
    {
        if (slider.value <= slider.minValue)
        {
            fillImage.enabled = false;
        }

        if (slider.value > slider.minValue && !fillImage.enabled)
        {
            fillImage.enabled = true;
        }
        float fillValue = player.GetComponent<PlayerData>().CurrentLife / player.GetComponent<PlayerData>().MaxLife;
        slider.value = fillValue;
    }
}
