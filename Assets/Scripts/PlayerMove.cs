using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public CharacterController controller;
    public Animator animator;
    public Transform cam;

    [Header("Movement")]
    public float speed = 1f;
    public float gravity = -9.10f;
    public float jumpHeight = 1f;

    [Header("Ground Check")]
    public Transform ground_check;
    public float ground_distance = 0.4f;
    public LayerMask ground_mask;

    Vector3 velocity;
    bool isGrounded;

    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    public bool isCrouch = false;
    public bool isDancing = false;
    public bool isRolling = false;
    public bool moveAvailable = true;
    public bool isWeaponOut = false;

    void Update()
    {
        if (Input.GetKey(KeyCode.M))
        {
            Dance();
        }

        if (this.GetComponent<PlayerData>().playerDeath)
        {
            moveAvailable = false;
        }

        if (moveAvailable)
        {
            isGrounded = Physics.CheckSphere(ground_check.position, ground_distance, ground_mask);

            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }

            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            animator.SetFloat("forward", z, 0.1f, Time.deltaTime);
            animator.SetFloat("strafe", x, 0.1f, Time.deltaTime);

            //playerAnimator.SetFloat("Vertical", Input.GetAxisRaw("Vertical"), smoothBlend, Time.deltaTime);

            Vector3 move = transform.right * x + transform.forward * z;

            controller.Move(move * speed * Time.deltaTime);

            if (!this.GetComponent<AimController>().isAiming)
            {
                if (Input.GetKeyDown(KeyCode.C))
                {
                    if (isCrouch)
                    {
                        isCrouch = false;
                        animator.SetBool("crouch", false);
                    }
                    else
                    {
                        isCrouch = true;
                        animator.SetBool("crouch", true);
                    }
                }
            }
            

            if (Input.GetKey(KeyCode.LeftShift))
            {
                speed = 5f;
                animator.SetBool("run", true);

                Vector3 direction = new Vector3(x, 1.015f, z).normalized;

                if (direction.magnitude >= 0.1f)
                {
                    float targetAngle = Mathf.Atan2(direction.z, -direction.x) * Mathf.Rad2Deg - 90f + cam.eulerAngles.y;

                    float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
                    transform.rotation = Quaternion.Euler(0f, angle, 0f);
                    Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                    controller.Move(moveDir.normalized * speed * Time.deltaTime);
                }
            }
            else
            {
                speed = 3f;
                animator.SetBool("run", false);
            }

            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            }

            if (isGrounded)
            {
                animator.SetBool("jump", false);
            }
            else
            {
                animator.SetBool("jump", true);
            }

            velocity.y += gravity * Time.deltaTime;

            controller.Move(velocity * Time.deltaTime);

            /*if (Input.GetKey(KeyCode.R) && !isRolling)
            {
                isRolling = true;
                animator.SetBool("roll", true);
                
            }
            else
            {
                isRolling = false;
                animator.SetBool("roll", false);
            }*/
        }
    }

    public void Dance()
    {
        isDancing = true;
        //Debug.Log("IsDancing: " + isDancing);
        isDancing = false;
        StartCoroutine(DanceTime());
    }

    private IEnumerator DanceTime()
    {
        /*int danceType = Random.Range(0, 6);
        int seconds;*/
        moveAvailable = false;
        /*animator.SetFloat("DanceType", danceType);
        switch (danceType)
        {
            case 1:
                seconds = 11;
                break;
            case 2:
                seconds = 9;
                break;
            case 3:
                seconds = 7;
                break;
            case 4:
                seconds = 1;
                break;
            case 5:
                seconds = 6;
                break;
            default:
                break;
        }*/
        animator.SetBool("dance", true);
        yield return new WaitForSeconds(11);
        moveAvailable = true;
        animator.SetBool("dance", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Item")
        {
            animator.SetTrigger("pickup");
            if (this.GetComponent<PlayerData>().CurrentLife + 30 <= 100)
            {
                this.GetComponent<PlayerData>().CurrentLife += 30f;
            }
            else
            {
                this.GetComponent<PlayerData>().CurrentLife = 100f;
            }
        }
    }
}
