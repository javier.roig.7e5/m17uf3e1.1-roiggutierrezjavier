using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyNavMesh : MonoBehaviour
{
    public Transform[] waypoints;
    public int currentWaypoint = 0;
    [SerializeField] public Transform playerTransform;
    public float distance;
    private NavMeshAgent navMeshAgent;
    private Animator animator;
    public bool EnemyDead = false;
    private bool patrolAvailable = true;
    public bool walking = false;
    private bool ShootAvailable = true;
    public GameObject projectile;
    public Transform firePoint;
    public float projectileSpeed = 10f;
    public bool Detected = false;
    public float Health = 5;
    [SerializeField] private GameObject WinController;
    private bool alreadyDead = false;

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        StartCoroutine(PatrolWaitTime());
    }

    void Update()
    {
        if (Health < 1)
        {
            EnemyDead = true;
        }

        if (!EnemyDead)
        {
            distance = Vector3.Distance(transform.position, playerTransform.position);
        }
        else
        {
            distance = 100f;
            Detected = false;
            if (!alreadyDead)
            {
                StartCoroutine(WinScreenTime());
                alreadyDead = true;
            }
        }
        animator.SetFloat("Distance", distance);
        animator.SetBool("Detected", Detected);
        animator.SetBool("Dead", EnemyDead);
    }

    public void Alert()
    {
        navMeshAgent.speed = 0f;
    }


    public void Patrol()
    {
        if (currentWaypoint < waypoints.Length && patrolAvailable)
        {
            navMeshAgent.destination = waypoints[currentWaypoint].position;
            if (transform.position.x == waypoints[currentWaypoint].position.x && transform.position.z == waypoints[currentWaypoint].position.z)
            {
                currentWaypoint++;
                StartCoroutine(PatrolWaitTime());
            }
        }
        else if (currentWaypoint >= waypoints.Length)
        {
            currentWaypoint = 0;
        }
    }

    public void Chase()
    {
        navMeshAgent.destination = playerTransform.position;
    }

    public void Attack()
    {
        if (ShootAvailable)
        {
            var bullet = Instantiate(projectile, firePoint.position, firePoint.rotation);
            bullet.GetComponent<Rigidbody>().velocity = firePoint.forward * projectileSpeed;
            StartCoroutine(Cadence());
        }
    }

    private IEnumerator PatrolWaitTime()
    {
        patrolAvailable = false;
        walking = false;
        yield return new WaitForSeconds(4);
        walking = true;
        patrolAvailable = true;
    }

    public IEnumerator Cadence()
    {
        ShootAvailable = false;
        yield return new WaitForSeconds(2);
        ShootAvailable = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            //animator.SetTrigger("Hitted");
            TakeDamage(1);
        }
    }

    private void TakeDamage(float damage)
    {
        Health -= damage;
    }

    private IEnumerator WinScreenTime()
    {
        yield return new WaitForSeconds(5);
        WinController.GetComponent<WinLose>().GameWin = true;
    }
}
