using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class DanceCam : MonoBehaviour
{
    [SerializeField] private CinemachineFreeLook thrirdPersonCam;

    void Update()
    {
        if (!this.GetComponent<PlayerMove>().moveAvailable)
        {
            thrirdPersonCam.gameObject.SetActive(false);
        }
        else if(this.GetComponent<PlayerMove>().moveAvailable)
        {
            thrirdPersonCam.gameObject.SetActive(true);
        }
    }
}
