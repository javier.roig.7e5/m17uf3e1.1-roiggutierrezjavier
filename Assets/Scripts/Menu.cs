using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("Test-R1.1.1");
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void PlayCamTest()
    {
        SceneManager.LoadScene("Test-R1.1.2");
    }

    public void Exit()
    {
        Debug.Log("Salir");
        Application.Quit();
    }
}
