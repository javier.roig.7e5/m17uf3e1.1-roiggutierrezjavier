M17UF3E1.1-RoigGutierrezJavier


CONTROLES DEL JUEGO
	- Moverse: W,A,S,D
	- Saltar: Space
	- Apuntar: Right Click
	- Disparar: Left Click
	- Desenfundar / Enfundar arma: Q
	- Bailar / Celebrar: M
	- Agacharse: C
	- Correr: Left Shift


FEATURES

	ENEMIGOS
		- Hay dos variantes estéticas de enemigos (Uniforme Blanco o Azul)
		- Tienen tres variantes de comportamiento:
			- Idle: si estas muy lejos
			- Persecución: si entras dentro de su rango de visión
			- Disparo: cuando están a menos de 6 metros de ti empezarán a dispararte
				- Hay dos variantes de disparo que es aleatoria en cada enemigo cuando spawnea (De pie o Agachado)
		- Cada enemigo tiene 5 de vida y quitas 1 por cada disparo
		- Disponen de 5 animaciones de muerte disponible que se ejecutan de forma aleatoria
		- Cuando mueren su cuerpo permanece en el suelo 10 segundos antes de destruirse
		- Tiene animaciones de Idle, Caminar, Disparo (de pie / agachado), recibir daño (2 variantes) y muerte
		- Tienen un cooldown de 2 segundos entre disparos
		- Hacen 15 de daño por disparo
		- Cuando el jugador muere dejan de dispararle pero siguen apuntándole
		- Spawner:
			- Spawnean en oleadas de entre 2 y 8 enemigos por oleada (aleatorio)
			- Las oleadas son infinitas y tienen un tiempo de 20 segundos entre oleada y la siguiente
			- En la parte superior del HUD puedes ver la oleada actual y los enemigos eliminados hasta el momento
			- La partida se gana cuando eliminas al limite de enemigos establecidos (por defecto 25), que se puede cambiar desde el editor
			- Cuando el jugador alcanza el numero de muertes los enemigos restantes que queden en la zona se destruyen y el spawner se detiene

	JUGADOR
		- En la parte superior del HUD puede ver reflejada la cantida de vida, la oleada actual y los enemigos eliminados
		- Puede recuperar vida recogiendo los botiquines que hay por el suelo del mapa hacercandose a ellos
		- Cuando recibe un impacto aparecera un efecto visual de sangre en la pantalla que representa el daño
		- Puede moverse hacia delante y atrás, y hacia los lados con animaciones diferentes segun la dirección y orientación del personaje, lo mismo sucede cuando se mueve agachado o apuntando
		- Puedes rotar la orientación hacia donde mira el jugador manteniendo el "LEFT SHIFT"
		-  Cuando te agachas no puedes ni apuntar ni disparar, ni puedes agacharte mientras apuntas
		- Cuando apuntas cambia la camara y aparaece un puntero que indica la dirección en la que apuntas
		- La bala no directamente donde esta la mira, si no un poco más abajo
		- Las balas salen del cañon del arma, asi que si disparas mientras te mueves o corres el dispara saldrá desviado
		- La munición es infinita y no hace falta recargar
		- La cámara es libre pero cuando apuntas se vuelve fija
		- Cuando esta apuntando las animaciones de caminar son diferentes (Animation Layers)
		- Tiene animaciones de caminar (distintas direcciones), Correr, Saltar, Agacharse, Caminar Agachado, Interactuar (agarrar item), Muerte, Baile, Apuntar, Caminar Apuntando, etc.


ESCENAS
	- Test-R1.1.1
		- Es el modo survival
		- Mapa de ciudad abandonada, solo tiene colisiones en el suelo
		- Hay un total de 10 botiquines para curar al jugador repartidos por el mapa
		- Si sales de los limites del mapa caeras al vacío
		- Cuando mueres o ganas la partida, te aparecerá la pantalla correspondiente y tres botones para reiniciar la partida, ir al modo Test de la cámara o salir de la aplicación
		- Cuando ganas la partida el personaje empezará a bailar y no podrás manejarlo
	- Test-R1.1.2
		- Es un escenario de pruebas con algunos elementos para probar las físicas de salto y la colisión de la cámara con los objetos de la escena
		- Si ejecutas un baile con la tecla M la cámara cambiará por una estática que enfoca de frente al personaje, y este no se podrá mover hasta que termine el baile
		- En la parte superior tendrás tres botones para, reiniciar la escena, ir al modo survival o salir de la aplicación


DETALLES
	- La versión final no usa el Input System por que me dio algunos problemas
	- En la propia escena hay una variante del personaje del jugador llamada PlayerPoolInputSystem, que contiene el input manager y algunos scripts específicos usando el Input System. Pero no llega a ser funcional.